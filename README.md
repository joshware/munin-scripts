# Installation Instructions


Add to the end of /etc/munin/plugin-conf.d/munin-node

    [aac_*]
    user root

    [minecraft_*]
    env.hostname localhost  # optional
    env.port 25575  # optional
    env.password rcon_password

    [iinet_*]
    env.username name
    env.password abc123
    env.base_url http://alternate-server/ # optional


Link/copy files into /etc/munin/plugins